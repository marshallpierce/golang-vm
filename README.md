A Vagrantfile for setting up an Ubuntu VM with go installed and the necessary env vars set.

- `vagrant up`
- `vagrant ssh`
- `cd go/src`
- `git clone ...`
- Copy out anything you want into `/vagrant` in the guest and it will be in this host dir.
